﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics.Eventing.Reader;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using Tp3_A21.Properties;

namespace Tp3_A21
{
    public partial class frmPrincipal : Form
    {
        private string route = "";

        public frmPrincipal()
        {
            InitializeComponent();
            btnMonter.Image = new Bitmap(Resources.arrowup, btnMonter.Width, btnMonter.Height);
            btnDescendre.Image = new Bitmap(Resources.arrowdown, btnDescendre.Width, btnDescendre.Height);
        }

        #region A faire


        private void ouvrirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Fichiers html (*.html)|*.html";
            ofd.DefaultExt = "html";
            tvHTML.Nodes.Clear();
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                StreamReader sr = new StreamReader(ofd.FileName);
                route = ofd.FileName;
                string htmlInitial = sr.ReadToEnd().Trim();
                sr.Close();
                string html = Regex.Replace(htmlInitial, @"\t|\n|\r", "");
                string[] lesBalises = SeparerLesBalises(html);
                lesBalises = lesBalises.Skip(1).ToArray();
                if (RetournerPremiereBalise(lesBalises).Nom[0] == '!')
                {
                    tvHTML.Nodes.Add(RetournerPremiereBalise(lesBalises));
                    lesBalises = lesBalises.Skip(1).ToArray();
                }
                tvHTML.Nodes.Add(CreerHtmlTree(lesBalises, RetournerPremiereBalise(lesBalises)));
            }
        }
        
        /// <summary>
        /// Retourne la premiere balise d'un vecteur de balises.
        /// </summary>
        /// <param name="lesBalises">Les balises HTML.</param>
        /// <returns>Retourne la premiere balise.</returns>
        private Balise RetournerPremiereBalise(string[] lesBalises)
        {
            Balise baliseInitiale;
            Dictionary<string, string> attributs = new Dictionary<string, string>();
            string strBalise = "";
            if (lesBalises.Length > 0)
                strBalise = lesBalises[0];
            string[] baliseSeparee = strBalise.Split('>');
            string nom = "";
            string contenu = "";
            if (baliseSeparee.Length >= 2)
            {
                contenu = baliseSeparee[1];
                if (baliseSeparee[0].Contains('='))
                {
                    string[] baliseInterieureSeparee = baliseSeparee[0].Split(" ");
                    nom = baliseInterieureSeparee[0];
                    if (baliseInterieureSeparee.Length > 1)
                    {
                        for (int i = 1; i < baliseInterieureSeparee.Length; i++)
                        {
                            if (baliseInterieureSeparee[i] != "")
                            {
                                string[] monAttribut = baliseInterieureSeparee[i].Split('=');
                                monAttribut[1] = monAttribut[1].Trim('"');
                                attributs.Add(monAttribut[0], monAttribut[1]);
                            }
                        }
                    }
                }
                else
                {
                    nom = baliseSeparee[0];
                }
            }

            if (nom != "")
                baliseInitiale = new Balise(nom, attributs, contenu);
            else
            {
                Balise baliseNulle = new Balise("Null", attributs, "null");
                return baliseNulle;
            }
            return baliseInitiale;
        }

        private string[] SeparerLesBalises(string pHtml)
        {
            return pHtml.Split('<');
        }

        private TreeNode CreerHtmlTree(string[] lesBalises, Balise noeudCourant)
        {
            if (lesBalises.Length != 1)
            {
                Balise maBalise = RetournerPremiereBalise(lesBalises);

                if (maBalise.Nom[0] == '/')
                {
                    noeudCourant = noeudCourant.Parent as Balise;
                }
                else if (maBalise.Nom[maBalise.Nom.Length - 1] == '/')
                {
                    noeudCourant.Nodes.Add(maBalise);
                }
                else
                {
                    noeudCourant.Nodes.Add(maBalise);
                    noeudCourant = maBalise;
                }
                string[] lesBalisesReduites = lesBalises.Skip(1).ToArray();
                return CreerHtmlTree(lesBalisesReduites, noeudCourant);
            }
            return noeudCourant;
        }

        private void enregistrerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (route != "")
            {
                List<string> mesBalisesHtml = ExporterArbre(tvHTML);
                StreamWriter sw = new StreamWriter(route);

                foreach (string balise in mesBalisesHtml)
                {
                    sw.Write(balise);
                }
                sw.Close();
            }
            else
            {
                enregistrerSousToolStripMenuItem_Click(sender, e);
            }
            
        }

        private List<string> ExporterArbre(TreeView arbre)
        {
            List<string> mesBalisesHtml = new List<string>();
            foreach (Balise parent in arbre.Nodes)
            {
                mesBalisesHtml.AddRange(parent.RetournerHtmlSelfEtEnfants());
            }

            return mesBalisesHtml;
        }

        private void enregistrerSousToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Fichiers html (*.html)|*.html";
            sfd.DefaultExt = "html";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                List<string> mesBalisesHtml = ExporterArbre(tvHTML);
                StreamWriter sw = new StreamWriter(sfd.FileName);

                foreach (string balise in mesBalisesHtml)
                {
                    sw.Write(balise);
                }
                sw.Close();
            }
        }

        private void btnRechParId_Click(object sender, EventArgs e)
        {
            string id = txtRechID.Text;
            ParcourirArbreId(id);
        }


        private void btnRechParTags_Click(object sender, EventArgs e)
        {
            string tag = txtRechTags.Text;
            lbRechParTag.Items.Clear();
            RechercheParTag(tag);
        }

        private void RechercheParTag(string tag)
        {
            List<Balise> lesBalises = new List<Balise>();
            foreach (Balise parent in tvHTML.Nodes)
            {
                lesBalises = parent.TrouverParTag(tag, lesBalises);
            }

            foreach (Balise balise in lesBalises)
            {
                lbRechParTag.Items.Add(balise);
            }
        }

        private void ParcourirArbreId(string id)
        {
            bool bTrouve = false;
            int i = 0;
            while (!bTrouve && i < tvHTML.Nodes.Count)
            {
                Balise maBalise = ((Balise)tvHTML.Nodes[i]).TrouverParId(id);
                if (maBalise != null)
                {
                    bTrouve = true;
                    tvHTML.SelectedNode = maBalise;
                }

                i++;
            }

            if (!bTrouve)
                tvHTML.SelectedNode = null;
            tvHTML.Focus();
        }

        private void btnMonter_Click(object sender, EventArgs e)
        {
            if (tvHTML.SelectedNode != null)
            {
                TreeNode parent = tvHTML.SelectedNode.Parent;
                if (parent != null)
                {
                    int index = parent.Nodes.IndexOf(tvHTML.SelectedNode);
                    if (index > 0)
                    {
                        Balise baliseBougee = tvHTML.SelectedNode as Balise;
                        parent.Nodes.RemoveAt(index);
                        parent.Nodes.Insert(index - 1, baliseBougee);
                    }
                }
                else if (tvHTML.Nodes.Contains(tvHTML.SelectedNode))
                {
                    int index = tvHTML.Nodes.IndexOf(tvHTML.SelectedNode);
                    if (index > 0)
                    {
                        Balise baliseBougee = tvHTML.SelectedNode as Balise;
                        tvHTML.Nodes.RemoveAt(index);
                        tvHTML.Nodes.Insert(index - 1, baliseBougee);
                    }
                }
            }
            tvHTML.SelectedNode = null;
        }

        private void btnDescendre_Click(object sender, EventArgs e)
        {
            if (tvHTML.SelectedNode != null)
            {
                TreeNode parent = tvHTML.SelectedNode.Parent;
                if (parent != null)
                {
                    int index = parent.Nodes.IndexOf(tvHTML.SelectedNode);
                    if (index < parent.Nodes.Count - 1)
                    {
                        Balise baliseBougee = tvHTML.SelectedNode as Balise;
                        parent.Nodes.RemoveAt(index);
                        parent.Nodes.Insert(index + 1, baliseBougee);
                    }
                }
                else if (tvHTML != null && tvHTML.Nodes.Contains(tvHTML.SelectedNode))
                {
                    int index = tvHTML.Nodes.IndexOf(tvHTML.SelectedNode);
                    if (index < tvHTML.Nodes.Count - 1)
                    {
                        Balise baliseBougee = tvHTML.SelectedNode as Balise;
                        tvHTML.Nodes.RemoveAt(index);
                        tvHTML.Nodes.Insert(index + 1, baliseBougee);
                    }
                }
            }
            tvHTML.SelectedNode = null;
        }

        #endregion


        #region Menu

        private void nouveauToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tvHTML.Nodes.Clear();
            route = "";
            tvHTML.Nodes.Add(new Balise("html"));
            tvHTML.Nodes[0].Nodes.Add(new Balise("head"));
            tvHTML.Nodes[0].Nodes.Add(new Balise("body"));
        }

        private void ajouterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ValidationHTML())
            {
                frmAttribut formSecondaire = new frmAttribut();
                if (formSecondaire.ShowDialog() == DialogResult.OK)
                {
                    if (!((Balise) tvHTML.SelectedNode).Attributs.ContainsKey(formSecondaire.Cle))
                    {
                        ((Balise) tvHTML.SelectedNode).Attributs.Add(formSecondaire.Cle, formSecondaire.Valeur);
                        AffichageAttributs();
                        tvHTML.SelectedNode.Text = tvHTML.SelectedNode.ToString();
                    }
                    else
                    {
                        MessageBox.Show("Attribut déjà présent dans cet élément");
                    }
                }
            }
        }

        private void fermerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ajouterUnElementToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RetirerErreurs();
            frmBalise formSecondaire = new frmBalise();
            if (formSecondaire.ShowDialog() == DialogResult.OK)
            {
                if (tvHTML.SelectedNode == null)
                {
                    if (tvHTML.Nodes.Count > 0)
                    {
                        errorProvider1.SetError(tvHTML,
                            "Impossible d'avoir deux noeuds racines dans le fichier. Veuillez sélectionner une balise HTML");
                    }
                    else
                        tvHTML.Nodes.Add(formSecondaire.Balise);
                }
                else
                {
                    tvHTML.SelectedNode.Nodes.Add(formSecondaire.Balise);
                }
            }
        }

        private void supprimerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ValidationHTML())
            {
                tvHTML.SelectedNode.Remove();
            }
        }

        private void supprimerToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (ValidationAttribut())
            {
                ((Balise) tvHTML.SelectedNode).Attributs.Remove(
                    ((KeyValuePair<string, string>) lbAttributs.SelectedItem).Key);
                AffichageAttributs();
            }
        }

        #endregion

        #region EvenementUI

        private void btnSauvContenu_Click(object sender, EventArgs e)
        {
            if (ValidationHTML())
            {
                ((Balise) tvHTML.SelectedNode).Contenu = txtContenu.Text;
                tvHTML.Focus();
            }
        }

        private void tvHTML_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (ValidationHTML())
            {
                AffichageAttributs();
                txtContenu.Text = ((Balise) tvHTML.SelectedNode).Contenu;
            }
        }

        private void AffichageAttributs()
        {
            lbAttributs.Items.Clear();
            if (tvHTML.SelectedNode != null)
            {
                foreach (KeyValuePair<string, string> attribut in ((Balise) tvHTML.SelectedNode).Attributs)
                {
                    lbAttributs.Items.Add(attribut);
                }
            }
        }

        private void lbRechParTag_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbRechParTag.SelectedIndex != -1)
            {
                tvHTML.SelectedNode = (Balise) lbRechParTag.SelectedItem;
                tvHTML.Focus();
            }
        }

        #endregion

        #region ValidationUI

        private void RetirerErreurs()
        {
            errorProvider1.SetError(tvHTML, "");
            errorProvider1.SetError(lbAttributs, "");
        }

        private bool ValidationHTML()
        {
            RetirerErreurs();
            if (tvHTML.SelectedNode == null)
            {
                errorProvider1.SetError(tvHTML, "Veuillez sélectionner une balise HTML");
                return false;
            }

            return true;
        }

        private bool ValidationAttribut()
        {
            RetirerErreurs();
            if (lbAttributs.SelectedIndex == -1)
            {
                errorProvider1.SetError(lbAttributs, "Veuillez sélectionner un attribut");
                return false;
            }

            return true;
        }

        #endregion
    }
}