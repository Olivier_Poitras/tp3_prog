﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tp3_A21
{
    public class Balise : TreeNode
    {
        private string _nom;
        private Dictionary<string, string> _attributs;
        private string _contenu;

        public string Nom
        {
            get { return _nom; }
            set { _nom = value; }
        }

        public Dictionary<string, string> Attributs
        {
            get { return _attributs; }
            set { _attributs = value; }
        }

        public string Contenu
        {
            get { return _contenu; }
            set { _contenu = value; }
        }

        public Balise(string pNom)
        {
            Nom = pNom;
            Attributs = new Dictionary<string, string>();
            Text = ToString();
        }

        public Balise(string pNom, Dictionary<string, string> pAttributs, string pContenu) : this(pNom)
        {
            _attributs = pAttributs;
            _contenu = pContenu;
            Text = ToString();
        }

        public override string ToString()
        {
            return "<" + Nom + ">" + " (" + Attributs.Count + " attributs)";
        }

        public Balise ComparerParTag(string tag)
        {
            if (tag == this.Nom)
                return this;
            else return null;
        }

        public Balise TrouverParId(string id)
        {
            if (Attributs.ContainsKey("id"))
            {
                if (Attributs["id"] == id)
                    return this;
            }
            else
            {
                foreach (Balise enfant in Nodes)
                {
                    Balise monEnfant = enfant.TrouverParId(id);
                    if (monEnfant != null)
                        return monEnfant;
                }
            }

            return null;
        }

        public List<Balise> TrouverParTag(string tag, List<Balise> lesBalises)
        {
            if (Nom == tag)
                lesBalises.Add(this);
            foreach (Balise enfant in this.Nodes)
                enfant.TrouverParTag(tag, lesBalises);
            return lesBalises;
        }

        public List<string> RetournerHtmlSelfEtEnfants()
        {
            List<string> mesBalisesEnfants = new List<string>();
            
            string mesAttributs = "";
            foreach (string attribut in Attributs.Keys)
            {
                mesAttributs += $" {attribut}=\"{Attributs[attribut]}\"";
            }
            mesBalisesEnfants.Add($"<{Nom}{mesAttributs}>{Contenu}");

            if (Nodes.Count != 0)
            {
                foreach (Balise enfant in Nodes)
                {
                    mesBalisesEnfants.AddRange(enfant.RetournerHtmlSelfEtEnfants());
                }
                mesBalisesEnfants.Add($"</{Nom}>");
            }
            else if (Nom[^1] != '/')
            {
                mesBalisesEnfants.Add($"</{Nom}>");
            }
            return mesBalisesEnfants;
        }
    }
}